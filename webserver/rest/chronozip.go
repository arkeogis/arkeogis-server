/* ArkeoGIS - The Geographic Information System for Archaeologists
 * Copyright (C) 2015-2016 CROLL SAS
 *
 * Authors :
 *  Nicolas Dimitrijevic <nicolas@croll.fr>
 *  Christophe Beveraggi <beve@croll.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package rest

import (
	"archive/zip"
	"bytes"
	"encoding/csv"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"log"
	"net/http"
	"reflect"
	"regexp"
	"strconv"
	"strings"

	"github.com/lib/pq"
	db "gitlab.huma-num.fr/arkeogis/arkeogis-server/db"
	"gitlab.huma-num.fr/arkeogis/arkeogis-server/model"
	routes "gitlab.huma-num.fr/arkeogis/arkeogis-server/webserver/routes"
)

const ChronoCsvColumnId = 0
const ChronoCsvColumnName1 = 1
const ChronoCsvColumnStart1 = 2
const ChronoCsvColumnStop1 = 3
const ChronoCsvColumnName2 = 4
const ChronoCsvColumnStart2 = 5
const ChronoCsvColumnStop2 = 6
const ChronoCsvColumnName3 = 7
const ChronoCsvColumnStart3 = 8
const ChronoCsvColumnStop3 = 9
const ChronoCsvColumnName4 = 10
const ChronoCsvColumnStart4 = 11
const ChronoCsvColumnStop4 = 12
const ChronoCsvColumnIdPeriodo = 13
const ChronoCsvColumnIdPactols = 14
const ChronoCsvColumnDescription = 15

type ChronoZipUpdateStruct struct {
	ChronoId   int    `json:"chronoId"`
	ZipContent []byte `json:"zipContent"`
}

func init() {
	Routes := []*routes.Route{
		&routes.Route{
			Path:        "/api/chronozip",
			Description: "Create/Update a chronology",
			Func:        ChronoUpdateZip,
			Method:      "POST",
			Json:        reflect.TypeOf(ChronoZipUpdateStruct{}),
			Permissions: []string{
				"request map",
			},
		},
	}
	routes.RegisterMultiple(Routes)
}

// ChronoUpdateZip Create/Update all chronologies from a zip file containing multiple languages
func ChronoUpdateZip(w http.ResponseWriter, r *http.Request, proute routes.Proute) {

	// get the post
	c := proute.Json.(*ChronoZipUpdateStruct)

	// transaction begin...
	tx, err := db.DB.Beginx()
	if err != nil {
		userSqlError(w, err)
		return
	}

	// get the user
	_user, ok := proute.Session.Get("user")
	if !ok {
		log.Println("ChronoUpdate: can't get user in session...", _user)
		_ = tx.Rollback()
		return
	}

	user, ok := _user.(model.User)
	if !ok {
		log.Println("ChronoUpdate: can't cast user...", _user)
		_ = tx.Rollback()
		return
	}

	err = user.Get(tx)
	user.Password = "" // immediatly erase password field, we don't need it
	if err != nil {
		log.Println("ChronoUpdate: can't load user...", _user)
		userSqlError(w, err)
		_ = tx.Rollback()
		return
	}

	chronoroot := model.Chronology_root{
		Root_chronology_id: c.ChronoId,
	}
	// search the chronology root to verify permissions
	if c.ChronoId > 0 {

		err = chronoroot.Get(tx)
		if err != nil {
			userSqlError(w, err)
			_ = tx.Rollback()
			return
		}

	}

	// take the group
	group := model.Group{
		Id: chronoroot.Admin_group_id,
	}
	err = group.Get(tx)
	if err != nil {
		userSqlError(w, err)
		_ = tx.Rollback()
		return
	}

	/**
	*
	* Job Here
	*
	**/

	/**** unzip ****/

	contents, err := readZipChrono(c.ZipContent)
	if err == nil {
		log.Println("no error reading zipfile")
	}
	if err != nil {
		log.Println("ChronoUpdateZip: can't load zip...", err)
		//fmt.Println("zip content : ", c.ZipContent)
		_ = tx.Rollback()
		routes.FieldError(w, "json.zipcontent", "zipcontent", err.Error())
		return
	}

	if len(contents) != 2 {
		log.Println("ChronoUpdateZip: contents != 2", len(contents))
		_ = tx.Rollback()
		routes.FieldError(w, "json.zipcontent", "zipcontent", "CHRONO.FIELD_CONTENT.T_BADCOUNT")
		return
	}

	answer, err := chronologiesGetTree(tx, c.ChronoId, user)
	if err != nil {
		log.Println("ChronoUpdateZip: chronologiesGetTree failed...", err)
		_ = tx.Rollback()
		routes.FieldError(w, "json.zipcontent", "internal error", err.Error())
		return
	}

	err = csvzipDoTheMixChrono(answer, contents)
	if err != nil {
		log.Println("ChronoUpdateZip: csvzipDoTheMix failed...", err)
		_ = tx.Rollback()
		chronozipError(w, err)
		return
	}

	// save recursively this chronology
	err = setChronoRecursive(tx, &answer.ChronologyTreeStruct, nil)
	if err != nil {
		userSqlError(w, err)
		_ = tx.Rollback()
		return
	}

	// commit...
	err = tx.Commit()
	if err != nil {
		log.Println("commit failed")
		chronozipError(w, err)
		_ = tx.Rollback()
		return
	}

	j, err := json.Marshal(answer)
	if err != nil {
		log.Println("marshal failed: ", err)
	}
	//log.Println("result: ", string(j))
	w.Write(j)

}

func readZipChrono(zipContent []byte) (map[string]ZipContent, error) {
	reader, err := zip.NewReader(bytes.NewReader(zipContent), int64(len(zipContent)))

	if err != nil {
		log.Println("ChronoUpdateZip: can't load zip...", err)
		return nil, err
	}

	zipContents := map[string]ZipContent{}

	for _, file := range reader.File {
		if file.FileInfo().IsDir() {
			continue
		}

		content := ZipContent{}

		re := regexp.MustCompile(`-([a-z]{2})\.csv$`)
		matches := re.FindStringSubmatch(file.Name)
		if len(matches) != 2 {
			return nil, errors.New("Bad file name inside zip. Must be for example : \"Something-en.csv\" for english, etc.")
		}
		content.IsoCode = matches[1]

		fileReader, err := file.Open()

		if err != nil {
			return nil, err
		}
		defer fileReader.Close()

		buf := new(strings.Builder)
		_, err = io.Copy(buf, fileReader)
		if err != nil {
			return nil, err
		}
		content.CSV = buf.String()
		content.Decoded, err = csvDecodeChronos(content.CSV)

		if err != nil {
			return nil, err
		}

		// strip every fields
		for y, _ := range content.Decoded {
			for x, _ := range content.Decoded[y] {
				content.Decoded[y][x] = strings.TrimSpace(content.Decoded[y][x])
			}
		}

		zipContents[content.IsoCode] = content
	}

	return zipContents, nil

}

func csvzipDoTheMixChrono(actual *ChronologiesUpdateStruct, newcontent map[string]ZipContent) error {

	//fmt.Println("actual: ", actual)

	firstlang := "en"
	for lang, _ := range newcontent {
		firstlang = lang
		break
	}
	totalcount := len(newcontent[firstlang].Decoded)

	toremove := ChronologyTreeStruct{}

	for linenum := 1; linenum < totalcount; linenum++ {
		ids := map[string]string{}
		pactolsIds := map[string]string{}
		periodoIds := map[string]string{}
		description := map[string]string{}
		paths := map[string][]string{}

		for lang, zipContent := range newcontent {
			if len(zipContent.Decoded[linenum]) != (ChronoCsvColumnDescription + 1) {
				fmt.Println("line : ", zipContent.Decoded[linenum])
				printSlice(zipContent.Decoded[linenum])
				return errors.New(
					"Line " + strconv.Itoa(linenum) +
						" for language " + lang +
						" have " + strconv.Itoa(len(zipContent.Decoded[linenum])) +
						" columns instead of " + strconv.Itoa(ChronoCsvColumnDescription+1))
			}

			ids[lang] = zipContent.Decoded[linenum][ChronoCsvColumnId]
			pactolsIds[lang] = zipContent.Decoded[linenum][ChronoCsvColumnIdPactols]
			periodoIds[lang] = zipContent.Decoded[linenum][ChronoCsvColumnIdPeriodo]
			description[lang] = zipContent.Decoded[linenum][ChronoCsvColumnDescription]
			path := []string{}

			for y, val := range zipContent.Decoded[linenum][ChronoCsvColumnName1 : ChronoCsvColumnStop4+1] {
				if len(val) == 0 {
					break
				}
				path = append(path, zipContent.Decoded[linenum][ChronoCsvColumnName1+y])
			}
			paths[lang] = path
		}

		// check if every ids are identical
		for _, id := range ids {
			if id != ids[firstlang] {
				return errors.New("IDs on line " + strconv.Itoa(linenum) + " are not identical on all languages")
			}
		}

		// check if every pactolsIds are identical
		for _, pactolsId := range pactolsIds {
			if pactolsId != pactolsIds[firstlang] {
				return errors.New("pactolsIds on line " + strconv.Itoa(linenum) + " are not identical on all languages : '" + pactolsIds[firstlang] + "' != '" + pactolsId + "'")
			}
		}
		// check if periodIds are identical
		for _, periodoId := range periodoIds {
			if periodoId != periodoIds[firstlang] {
				return errors.New("periodoIds on line " + strconv.Itoa(linenum) + " are not identical on all languages : '" + periodoIds[firstlang] + "' != '" + periodoId + "'")
			}
		}
		// check if every paths heve same size
		for _, path := range paths {
			if len(path) != len(paths[firstlang]) {
				return errors.New("chronologies on line " + strconv.Itoa(linenum) + " do not have same levels on all languages")
			}
		}

		// do the update/insert
		if len(ids[firstlang]) > 0 {
			// we have an id, so it's an update action
			id, err := strconv.Atoi(ids[firstlang])
			if err != nil {
				return errors.New("bad ID on line " + strconv.Itoa(linenum))
			}

			// if id < 0, this is a delete action
			if id < 0 { // DELETE ACTION
				found := csvzipRemoveChronoByID(&actual.ChronologyTreeStruct, -id, nil, -1)
				if found != nil {
					log.Println("removed " + strconv.Itoa(found.Id))
					toremove.Content = append(toremove.Content, *found)
				} else {
					leaffound := csvzipRemoveChronoByID(&toremove, -id, nil, -1)
					if leaffound != nil {
						log.Println("removed2 " + strconv.Itoa(leaffound.Id))
						toremove.Content = append(toremove.Content, *leaffound)
					} else {
						return errors.New("chronologies on line " + strconv.Itoa(linenum) + " with id " + strconv.Itoa(-id) + " was not found for removing, or is a LVL1 chronology and therefore cannot be removed")
					}
				}
			} else { // UPDATE ACTION
				elem := csvzipSearchChronoByID(&actual.ChronologyTreeStruct, id)

				if elem == nil {
					return errors.New("chronologies on line " + strconv.Itoa(linenum) + " with id " + strconv.Itoa(id) + " was not found for updating")
				}

				elem.Chronology.Id_ark_pactols = pactolsIds[firstlang]
				elem.Chronology.Id_ark_periodo = periodoIds[firstlang]

				for lang, _ := range newcontent {
					if _, ok := elem.Name[lang]; ok {
						//update
						elem.Name[lang] = paths[lang][len(paths[lang])-3]
						elem.Chronology.Start_date = dateToDb(paths[lang][len(paths[lang])-2])
						elem.Chronology.End_date = dateToDb(paths[lang][len(paths[lang])-1])
						elem.Description[lang]= description[lang]
					}
				}
			}
		} else {
			// we no not have an id, so it's an insert action

			// create a new element
			subelem := ChronologyTreeStruct{}
			subelem.Chronology.Id_ark_pactols = pactolsIds[firstlang]
			subelem.Chronology.Id_ark_periodo = periodoIds[firstlang]
			subelem.Chronology.Start_date = dateToDb(paths[firstlang][len(paths[firstlang])-2]) 
			subelem.Chronology.End_date = dateToDb(paths[firstlang][len(paths[firstlang])-1])
			subelem.Description =  map[string]string{}

			subelem.Name = map[string]string{}

			for lang, _ := range newcontent {
				subelem.Name[lang] = paths[lang][len(paths[lang])-3]
				subelem.Description[lang] = description[lang]
			}

			if len(paths[firstlang]) > 1 {
				// search the parent
				parent := csvzipSearchChronoByPath(&actual.ChronologyTreeStruct, firstlang, paths[firstlang])

				if parent != nil {
					subelem.Chronology.Parent_id = parent.Id
				}else {
					return errors.New("Parent element of line " + strconv.Itoa(linenum) + " not found")
				}

				//fmt.Println("INSERT: ", subelem)
				parent.Content = append(parent.Content, subelem)
			} else {

				//fmt.Println("INSERT ROOT: ", subelem)

				actual.Content = append(actual.Content, subelem)
			}

		}

	}

	// check that toremove does not contain leafs anymore
	for _, elem := range toremove.Content {
		if len(elem.Content) > 0 {
			return errors.New("You are trying to remove a line without removing all it's leafs. id : " + strconv.Itoa(elem.Id))
		}
	}

	return nil // no error !
}

func dateToDb(date string) int {
	number, err := strconv.Atoi(date)
	if err != nil {
		return -1
	}
	if number > 0 {
		return number
	}
	return number + 1
}

func csvzipRemoveChronoByID(elem *ChronologyTreeStruct, id int, parent *ChronologyTreeStruct, parentidx int) *ChronologyTreeStruct {
	if elem.Id == id {
		copy := *elem
		// remove from parent
		for i, _ := range parent.Content {
			// locate chronology in parent
			if parent.Content[i].Id == id {
				// if LVL1 content cannot remove
				if parent.Parent_id == 0 {
					return nil
				}else{
					// remove content
					head := parent.Content[:i]
					tail := parent.Content[i+1:]
					parent.Content = append(head, tail...)
					return &copy
				}
			}
		}
	}

	for i, _ := range elem.Content {
		found := csvzipRemoveChronoByID(&elem.Content[i], id, elem, i)
		if found != nil {
			return found
		}
	}

	return nil
}

func csvzipSearchChronoByPath(elem *ChronologyTreeStruct, lang string, path []string) (*ChronologyTreeStruct) {

	if _, ok := elem.Name[lang]; ok {
		// level 2, 3, or 4
			if len(path) > 3 {
				for i, _ := range elem.Content {
					if path[len(path)-6] == elem.Content[i].Name[lang] {
						return &elem.Content[i]
					}else {
						search := csvzipSearchChronoByPath(&elem.Content[i], lang, path)
						if search != nil {
							return search
						}
					}
				}
				return nil
			} else {
				// level 1, parent is root
				return elem
			}
		
	}

	return nil
}

func csvzipSearchChronoByID(elem *ChronologyTreeStruct, id int) (*ChronologyTreeStruct) {

	if elem.Id == id {
		return elem
	}

	for i, _ := range elem.Content {
		found := csvzipSearchChronoByID(&elem.Content[i], id)
		if found != nil {
			return found
		}
	}

	return nil
}

func chronozipError(w http.ResponseWriter, err error) {
	log.Printf("paf: %#v\n", err)
	if pgerr, ok := err.(*pq.Error); ok {
		log.Printf("pgerr: %#v\n", pgerr.Code.Name())
		switch pgerr.Code.Name() {
		case "foreign_key_violation":
			routes.FieldError(w, "json.zipcontent", "error", "Unable to update or delete a row, the row may be in use by a database")
			break
		default:
			log.Printf("unhandled postgresql error ! : %#v\n", pgerr)
			routes.FieldError(w, "json.zipcontent", "database error", err.Error())
		}
	} else {
		log.Println("not a postgresql error !", err)
		routes.FieldError(w, "json.zipcontent", "internal error", err.Error())
	}
}

func csvDecodeChronos(in string) ([][]string, error) {
	r := csv.NewReader(strings.NewReader(in))
	r.LazyQuotes = true
	r.Comma = ';'
	records, err := r.ReadAll()
	if err != nil {
		return nil, err
	}
	return records, nil
}
